package Global;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.function.Function;

public class Waits {
    public static WebElement wait(org.openqa.selenium.support.ui.Wait<WebDriver> wait, By Byxpath) {
        return wait.until(new Function<WebDriver, WebElement>() {
            @Override
            public WebElement apply(WebDriver driver) {
                return driver.findElement(Byxpath);
            }
        });
    }

    public static WebElement waitClickable(org.openqa.selenium.support.ui.Wait<WebDriver> wait, By Byxpath) {
        return wait.until(ExpectedConditions.elementToBeClickable(Byxpath));
    }

    public static WebElement waitEnabled(WebDriver driver,long miliSeconds, By Byxpath){

        long verificarCada = 500;
        long bucle = miliSeconds / verificarCada;

        if ((miliSeconds % verificarCada) > 0) bucle++;

        for (long i = 0; i < bucle; i++) {
            try {
                Thread.sleep(verificarCada);

                if (driver.findElement(Byxpath).isEnabled()) {
                    return driver.findElement(Byxpath);
                }

            } catch (Exception ne) {
                System.out.println("el boton '" + Byxpath + "' no existe, tiempo :" + ((i + 1) * verificarCada) / 1000.0 + " segundos");
                if (i >= (bucle - 1)) {
                    System.out.println("\n" + "El boton '" + Byxpath + "' no existe pasado los " + miliSeconds / 1000.0 + " segundos" + "\n");
                    return null;
                }
            }
        }
        System.out.println("\n" + "El boton '" + Byxpath + "' no esta disponible pasado los " + miliSeconds / 1000.0 + " segundos" + "\n");
        return null;
    }

    public static void waitFor(long miliSeconds){
        try {
            Thread.sleep(miliSeconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
