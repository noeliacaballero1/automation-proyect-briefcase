package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class BusquedaAvanzadaPac_Page {
    WebDriver driver;

    @FindBy (xpath = "//a[contains(text(), \"Búsqueda avanzada\")]")
    WebElement busquedaAvanzada_link ;

    @FindBy (xpath = "//input[contains(@title,\"Primer apellido\")]")
    WebElement apellido;

    @FindBy (xpath = "//i[@title=\"Sexo\"]/input")
    WebElement sexo;

    @FindBy (xpath = "//button[contains(text(), \"Buscar\")]")
    WebElement buscar_button;


    public BusquedaAvanzadaPac_Page(WebDriver driver){

        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void clickOnBusquedaAvanzadaButton() {
        busquedaAvanzada_link.click();
    }

    public void setApellido(String ape){
        apellido.click();
        apellido.sendKeys(ape);
    }

    public void setSexo(String sex) {
        sexo.sendKeys(sex);
    }

    public void clickOnBuscarButton(){
        buscar_button.click();
    }


}
