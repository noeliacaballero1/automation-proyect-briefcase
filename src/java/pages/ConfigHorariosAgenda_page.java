package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ConfigHorariosAgenda_page {

    @FindBy(xpath = "//button[@title=\"Botón que permite crear un nuevo horario\"]")
    WebElement boton_NuevoHorario;

    @FindBy(xpath = "//input[@class=\"##agendaSettings.label.txCodHN_5487204417287670717 z-textbox\"]")
    WebElement codigo_Input;

    @FindBy(xpath = "//input[@class=\"##agendaSettings.label.txDesHN_699166700716511195 z-textbox\"]")
    WebElement descripcion_Input;

    @FindBy(xpath = "//i[@title=\"Muestra la fecha de inicio del horario\"]/i")
    WebElement fechaInicio_datebox;

    @FindBy(xpath = "//i[@title=\"Muestra la fecha de fin del horario\"]/i")
    WebElement fechaFin_datebox;

    //Días habilitados

    @FindBy(xpath = "//span[@title=\"L\"]")
    WebElement lunes_checkbox;

    @FindBy(xpath = "//span[@title=\"M\"]")
    WebElement martes_checkbox;

    @FindBy(xpath = "//span[@title=\"X\"]")
    WebElement miercoles_checkbox;

    @FindBy(xpath = "//span[@title=\"J\"]")
    WebElement jueves_checkbox;

    @FindBy(xpath = "//span[@title=\"V\"]")
    WebElement viernes_checkbox;

    @FindBy(xpath = "//span[@title=\"S\"]")
    WebElement sabado_checkbox;

    @FindBy(xpath = "//span[@title=\"D\"]")
    WebElement domingo_checkbox;

    @FindBy(xpath = "//span[@title=\"Horario permite atender en festivos\"]")
    WebElement festivos_check;

    @FindBy(className = "class=\"z-bandbox-btn\"")
    WebElement festivos_combo;

    @FindBy(xpath = "//i[@title=\"Muestra la hora de inicio del horario\"]/input")
    WebElement horaInicio_input;

    @FindBy(xpath = "//i[@title=\"Muestra la hora de fin del horario\"]/input")
    WebElement horaFin_input;

    @FindBy(xpath = "//i[@title=\"Muestra el número de minutos que durará cada cita\"]/input")
    WebElement intervaloTurnos_input;

    @FindBy(xpath = "//span[@title=\"Muestra si hay un numero ilimitado de turnos\"]")
    WebElement turnosIlimitados_check;

    @FindBy(xpath = "//input[@title=\"Muestra el número máximo de citas a programar en el horario\"]")
    WebElement maxTurnos_input;

    @FindBy(xpath = "//span[@title=\"Citación grupal\"]/input")
    WebElement citGrupal_check;

    @FindBy(xpath = "//input[@title=\"Texto citación grupal\"]")
    WebElement numPacxGrupo_input;

    @FindBy(xpath = "//input[@title=\"Muestra el número de citas forzadas en total que puede contener el horario\"]")
    WebElement citForzadas_input;

    @FindBy(xpath = "//span[@title=\"Pedir dosier físico\"]/input")
    WebElement dosier_check;

    @FindBy(xpath = "//i[@title=\"Muestra las prestaciones de la sección\"]/i")
    WebElement prestaciones_locator;

    @FindBy(xpath = "//i[@title=\"Muestra las prestaciones configuradas para el horario\"]/i")
    WebElement prestacionesDefault_locator;

    @FindBy(xpath = "title=\"Selección de propietarios de la agenda\"")
    WebElement propietarios_locator;

    @FindBy (xpath = "//button[@title=\"Botón que permite cancelar la modificación o creación del horario\"]")
    WebElement cancelar_boton;

    @FindBy (xpath = "//div[2]/button[@title=\"Botón que permite guardar los detalles del horario\"]")
    WebElement guardar_buton;


}
