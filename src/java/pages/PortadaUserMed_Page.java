package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PortadaUserMed_Page {
    WebDriver driver;

    @FindBy (xpath = "//button[@class=\"z-button-os\"][@title=\"Hospitalización\"]")
    WebElement mod_Hosp;

    @FindBy (xpath = "//button[@class=\"z-button-os\"][@title=\"Urgencias\"]")
    WebElement mod_Urg;

    @FindBy (xpath = "//button[@class=\"z-button-os\"][@title=\"Consulta Externa\"]")
    WebElement mod_CEX;

    @FindBy(xpath = "//button[@class=\"z-button-os\"][@title=\"Quirófano\"]")
    WebElement mod_Quir;

    @FindBy (xpath = "//button[@class=\"z-button-os\"][@title=\"RIS\"]")
    WebElement mod_RIS;

    @FindBy (xpath = "//button[@class=\"z-button-os\"][@title=\"Codificación\"]")
    WebElement mod_Codif;

    @FindBy (xpath = "//button[@class=\"z-button-os\"][@title=\"Epidemiología\"]")
    WebElement mod_Epid;

    @FindBy (xpath = "//button[@class=\"z-button-os\"][@title=\"Dietología\"]")
    WebElement mod_Diet;

    @FindBy (xpath = "//button[@class=\"z-button-os\"][@title=\"Obstetricia\"]")
    WebElement mod_Obst;

    @FindBy (xpath = "//button[@class=\"z-button-os\"][@title=\"Atención Primaria\"]")
    WebElement mod_AtPrim ;

    @FindBy (xpath ="//button[@class=\"z-button-os\"][@title=\"CEYE\"]")
    WebElement mod_CEYE;

    @FindBy(xpath = "//button[@class=\"z-button-os\"][@title=\"Autorizaciones\"]")
    WebElement mod_Aut;

    public PortadaUserMed_Page(WebDriver driver){

        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void seleccionarArea(String nameArea){
        switch (nameArea){
            case "Hospitalización" :
                mod_Hosp.click();

            case "Urgencias" :
                mod_Urg.click();
                break;

            case "Consulta Externa" :
                mod_CEX.click();
                break;

            case "Quirófano":
                mod_Quir.click();
                break;

            case "RIS":
                mod_RIS.click();
                break;

            case "Codificación":
                mod_Codif.click();
                break;

            case "Epidemiología":
                mod_Epid.click();
                break;

            case "Dietología":
                mod_Diet.click();
                break;

            case "Obstetricia":
                mod_Obst.click();
                break;

            case "Atención Primaria":
                mod_AtPrim.click();
                break;

            case "CEYE":
                mod_CEYE.click();
                break;

            case "Autorizaciones":
                mod_Aut.click();
                break;
        }


    }

}
