package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class Antecedentes_page {

    WebDriver driver;

    @FindBy (xpath = "//button[@title=\"Antecedentes\"]")
    WebElement AntecedentesBoton;

    @FindBy (xpath = "//span[contains(text(), \"Alergias\")]")
    WebElement AlergiasPestaña;

    @FindBy (xpath = "//button[@title=\"Botón que inserta un nuevo registro de alergia\"]")
    WebElement NuevoBoton;

    @FindBy (xpath = "//i[@title=\"Muestra el grado de alergia\"]/input")
    WebElement TipodeDeclaracion_Combo;

    @FindBy (xpath = "//div/i[@title=\"Muestra la lista de alérgenos\"][@class=\"##allergenManager.filter.csbAllTypeHN z-combobox\"]/input")
    WebElement TipoDeCausal_Combo;

    @FindBy (xpath = "//input[@title=\"Frecuencia y último contacto con el alérgeno\"]")
    WebElement Frecuencia_input;

    @FindBy (xpath = "//i[@class=\"##5640864650168412316 z-bandbox\"]/input")
    WebElement causal_input;

    @FindBy (xpath = "//span[contains(text(), \"PARACETAMOL\")]")
    WebElement causal_PARACETAMOL;

    @FindBy (xpath = "//i[@class=\"##allergies.helpname.cbDeclarante z-combobox\"]/input")
    WebElement declarante_Combo;

    @FindBy (xpath = "//button[@class=\"smallBtn ##habits.button.save z-button-os\"]")
    WebElement Guardar_button;


    public Antecedentes_page(WebDriver driver){
        PageFactory.initElements(driver, this);
    }

    public void completarAntecedentes() throws Exception{
        AntecedentesBoton.click();
        AlergiasPestaña.click();
        NuevoBoton.click();
        TipodeDeclaracion_Combo.sendKeys("Alergia");
        TipoDeCausal_Combo.sendKeys("Principio activo");
        Frecuencia_input.sendKeys("1 vez al día");
        Thread.sleep(2000);
        causal_input.sendKeys("PARACETAMOL");
        Thread.sleep(2000);
        causal_PARACETAMOL.click();
        declarante_Combo.sendKeys("Médico");
        Guardar_button.click();

    }



}
