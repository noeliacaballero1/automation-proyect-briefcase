package pages;

import Global.Waits;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.concurrent.TimeUnit;


import org.openqa.selenium.support.ui.Select;

public class ConfigDetalleAgenda_page {
    //PESTAÑA "DETALLE DE LA AGENDA"

    WebDriver driver;
    private String diaActual;

    @FindBy(xpath = "//button[@title=\"Configuración\"]")
    WebElement Configuracion_boton;

    @FindBy(xpath = "//a[contains(text(),\"Administrativa\")]")
    WebElement Menu_AdministrativaIcon;

    @FindBy(xpath = "//div[contains(text(),\"Agendas\")]")
    WebElement Menu_Agendas;

    @FindBy(xpath = "//button[@title=\"Botón que permite generar una nueva agenda\"]")
    WebElement Boton_Nuevo;

    @FindBy(xpath = "//input[@class=\"z-combobox-inp bgInputObligatorio\"]") //Aquí le pasaré un input
    WebElement tipoDeAgenda_Combo;

    @FindBy(xpath = "//input[@title=\"Muestra el código característico del horario\"][@class=\"##agendaSettings.label.txCodHN_8764493938218300649 z-textbox bgInputObligatorio\"]")
    WebElement codigo_Input;

    @FindBy(xpath = "//input[@title=\"Muestra la descripción que caracteriza a la agenda\"]")
    WebElement DescripcionAgendaInput;

    @FindBy(xpath = "//i[@title=\"Muestra la fecha de inicio de la vigencia de la agenda\"]/input")
    WebElement fechaInicio;

    @FindBy(xpath = "//i[@title=\"Filtro de búsqueda por código y descripción de centro\"]/i")
    WebElement centroLocator;

    @FindBy(xpath = "//i[@title=\"Filtro de búsqueda por código y descripción del área\"]/i")
    WebElement areaLocator;

    @FindBy(id = "zk_comp_514-btn")
    WebElement servicioLocator;

    @FindBy(xpath = "//i[@title=\"Filtro de búsqueda por código y descripción de la sección\"]/i")
    WebElement seccionLocator;

    @FindBy(id = "zk_comp_559")
    WebElement propAtenderLocator;

    @FindBy(id="zk_comp_573-btn")
    WebElement propProgLocator;

    @FindBy(xpath = "//td[@class=\"##8447442658696105746 z-cell\"]/button[@title=\"Botón que permite guardar los detalles del horario\"]")
    WebElement guardarButton;

    public ConfigDetalleAgenda_page(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void Entrar_ConfigAgendas(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        Configuracion_boton.click();
        Menu_AdministrativaIcon.click();
        Menu_Agendas.click();

    }

    public void click_botonNuevo(){

        Boton_Nuevo.click();
        Waits.waitFor(3000);
    }

    public void completa_Datos_Oblig() throws Exception{
        //Tipo de agenda
        tipoDeAgenda_Combo.sendKeys("Agenda de consulta");

        //código de la agenda
        codigo_Input.sendKeys("ANC");

        //Descripción de la agenda
        DescripcionAgendaInput.sendKeys("Agenda de Prueba");

        //Fecha Inicial
        String fechaActual = devolverFecha();
        fechaInicio.click();
        Thread.sleep(2000);
        fechaInicio.sendKeys(fechaActual);

        //Seleccionar centro
        centroLocator.click();
        Select select_obj = new Select(centroLocator);
        select_obj.selectByVisibleText("Centro General - CENTRO_0");

        //seleccionar área
        areaLocator.click();
        seleccionarValorLocator(areaLocator);

        //seleccionar servicio
        servicioLocator.click();
        seleccionarValorLocator(servicioLocator);

        //seleccionar sección
        seccionLocator.click();
        seleccionarValorLocator(seccionLocator);

    }

    //Método para obtener la fecha actual
    private String devolverFecha() {
        Date date = new Date();
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        int anio  = localDate.getYear();
        int mes = localDate.getMonthValue();
        int dia   = localDate.getDayOfMonth();
        String fecha;
        switch (mes){
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                fecha = dia + "/0" + mes + "/" + anio;
                break;

            default:
                fecha = dia + "/" + mes + "/" + anio;
        }


        return fecha;

    }

    private void seleccionarValorLocator(WebElement webelement){
        Select select_obj = new Select(webelement);
        if(webelement==centroLocator){
            select_obj.selectByVisibleText("Centro General - CENTRO_0");
        }
        if(webelement==areaLocator){
            select_obj.selectByVisibleText("Consulta Externa");
        }
        else {
            select_obj.selectByVisibleText("Traumatología - TRAUMA");
        }
    }
}
