package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AgendaCEX_page {
    WebDriver driver;
    By citaUrgenteButton = By.xpath("//button[@class=\"ellipsisBtn ##appointmentWorkList.button.no_schedule_act.label z-button-os\"]");
    By AceptarButton = By.xpath("//td[contains(text(),\"Aceptar\")]");
    public AgendaCEX_page(WebDriver driver){
        this.driver = driver;

    }

    public void clickOnButtonCitaUrg(){
        driver.findElement(citaUrgenteButton).click();
    }
    public void clickOnButtonAceptar() {driver.findElement(AceptarButton).click();}
}
