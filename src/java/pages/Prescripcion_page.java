package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Prescripcion_page {
    WebDriver driver;

    @FindBy (xpath = "//button[contains(text(), \"Prescripción\")]")
    WebElement Prescripcion_menu;

    @FindBy (xpath = "//a[@id=\"zk_comp_28-a\"]")
    WebElement Prescripcion_submenu;

    @FindBy (xpath = "//i[@title=\"Tipo de tratamiento\"]/input")
    WebElement TipoDeTratamiento_input;

    @FindBy (xpath = "//span[contains(text(), \"Medicación general\")]")
    WebElement MedicacionGeneral_resul;

    @FindBy (xpath = "//i[@title=\"Elección de algún tratamiento\"]/input")
    WebElement Tratamiento_input;

    @FindBy (xpath = "//span[contains(text(), \"Paracetamol 1000\")]")
    WebElement PARACETAMOL_result;

    @FindBy (xpath = "//i[@title=\"Establece la Vía de administración\"]/input")
    WebElement ViaDeAdministracion_input;

    @FindBy (xpath = "//i[@title=\"Campo que permite informar intervalos de administración de frecuencia y veces/día\"]/input")
    WebElement IntervaloAdministracion_input;

    @FindBy (xpath = "//span[contains(text(), \"C/8h\")]")
    WebElement Frec_result;

    @FindBy (xpath = "//input[@title=\"Muestra la sospecha diagnóstica\"]")
    WebElement sospechaDiag_input;

    @FindBy (xpath = "//button[contains(text(), \"Prescribir\")]")
    WebElement Prescribir_boton;

    @FindBy (xpath = "//button[@class=\"smallBtn ##EhCOSPopupFilter.accept z-button-os\"]")
    WebElement Aceptar_button;

     public Prescripcion_page(WebDriver driver){
         PageFactory.initElements(driver, this);
     }

     public void CompletarFormularioPrescripcion() throws Exception{

         Prescripcion_menu.click();
         Thread.sleep(1000);
         Prescripcion_submenu.click();
         Thread.sleep(5000);
         TipoDeTratamiento_input.sendKeys("Medicación General");
         Thread.sleep(2000);
         MedicacionGeneral_resul.click();
         Thread.sleep(3000);
         Tratamiento_input.sendKeys("Paracetamol 1000mg");
         Thread.sleep(5000);
         PARACETAMOL_result.click();
         Thread.sleep(3000);
         ViaDeAdministracion_input.sendKeys("Oral");
         Thread.sleep(2000);
         IntervaloAdministracion_input.sendKeys("C/8h");
         Thread.sleep(2000);
         Frec_result.click();
         sospechaDiag_input.sendKeys("Prueba Automatización");
         Prescribir_boton.click();
         Thread.sleep(2000);
         Aceptar_button.click();


     }
}
