package pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class ListaDePacientes_page {

    WebDriver driver;

    @FindBy(xpath = "//div[contains(text(), \"Karla\")]")
    WebElement seleccionar_pac;

    @FindBy(xpath = "//div[@class=\"z-tabs-header\"]/ul/li[4]/a")
    WebElement cerrar_Lista;


    public ListaDePacientes_page(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public void seleccionarPaciente() {
        seleccionar_pac.click();
    }

    public void cerrarListadePacientes() {
        cerrar_Lista.click();
    }

}
