package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SeleccionDeAgenda_Page {

    WebDriver driver;

    @FindBy (xpath = "//input[@title=\"Permite ingresar el nombre de la agenda a buscar\"]")
    WebElement filtroDeBusqueda;

    @FindBy (xpath = "//span[@class=\"##agendaElement.checkHN z-checkbox\"]")
    WebElement agenda;

    @FindBy (xpath = "//button[@title=\"Botón que permite redirigir a la lista de pacientes de la agenda o las agendas seleccionadas\"]")
    WebElement Acceder_button;

    public  SeleccionDeAgenda_Page(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void filtrarBusqueda(){
        filtroDeBusqueda.sendKeys("Agenda Noe Nueva");

    }

    public void seleccionarAgenda() throws Exception{
        agenda.click();
        Thread.sleep(2000);
        Acceder_button.click();
    }

}
