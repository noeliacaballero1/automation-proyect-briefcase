package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;



public class Login_Page {


    WebDriver driver;

    java.util.Properties properties = new java.util.Properties();


    @FindBy (xpath = "//input[@class=\"required\"][@id=\"username\"]")
    WebElement usuario_input;

    @FindBy(xpath = "//input[@class=\"required\"][@type=\"password\"]")
    WebElement contrasena_input;

    @FindBy (xpath = "//input[@name=\"submit\"]")
    WebElement entrar_boton;

    public Login_Page(WebDriver driver){
        this.driver= driver;
        PageFactory.initElements(driver, this);
    }

    //navegators
    public void setUserName (String strUserName){

        usuario_input.sendKeys(strUserName);
    }

    public void setPassword(String strPass){
        contrasena_input.sendKeys(strPass);
    }

    public void clickOnLoginButton(){
        entrar_boton.click();
    }

    public void login(String user, String pass){
        setUserName(user);
        setPassword(pass);
        clickOnLoginButton();

    }


}
