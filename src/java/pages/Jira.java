package pages;


import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class Jira {

    WebDriver driver;

    @FindBy (xpath = "//a[contains(text(),\"Identificación\")]")
    WebElement link_identificacion;

    @FindBy (xpath = "//input[@name=\"os_username\"]")
    WebElement user;

    @FindBy (xpath = "//input[@name=\"os_password\"]")
    WebElement password;

    @FindBy (xpath = "//input[@name=\"login\"]")
    WebElement button_Entrar;

    @FindBy (xpath = "//div[@id=\"issuetype-single-select\"]/input[@class=\"text aui-ss-field ajs-dirty-warning-exempt\"]")
    WebElement typeOfRequest_input;

    @FindBy (xpath = "//select[@id=\"issuetype\"]/option[contains(text(), \"Correctivo\")]")
    WebElement Correctivo_option;

    @FindBy (xpath = "//input[@name=\"Next\"]")
    WebElement siguiente_button;

    @FindBy (xpath = "//textarea[@name=\"description\"]")
    WebElement input_Descripción;

    @FindBy (xpath = "//div[@class=\"field-group aui-field-componentspicker frother-control-renderer\"]/div[@class=\"jira-multi-select long-field\"]/textarea[@class=\"text long-field\"]")
    WebElement input_componente;

    @FindBy (xpath = "//select/option[@value=\"21984\"]")
    WebElement nombreProyecto_option;

    @FindBy (xpath = "//select[@name=\"customfield_15851\"]")
    WebElement centro_select;

    @FindBy (xpath = "//option[@value=\"21990\"]")
    WebElement ehCOS_QA_option;

    @FindBy (xpath = "//select[@name=\"customfield_15852\"]")
    WebElement Equipo_select;

    @FindBy (xpath = "//option[@value=\"21992\"]")
    WebElement producto_option;

    @FindBy (xpath ="//div/div[11]/div/div/div/div/table/tbody/tr/td/select")
    WebElement select_subtypeRequest;

    @FindBy (xpath = "//div/div[11]/div/div/div/div/table/tbody/tr/td/select/option[contains(text(),\"Funcional\")]")
    WebElement Funcional_option;

    @FindBy (xpath = "//textarea[@id=\"versions-textarea\"]")
    WebElement input_versionAfectada;

    @FindBy (xpath = "//div/select[@id=\"customfield_15860\"]")
    WebElement select_FaseDetecInc;

    @FindBy (xpath = "//select/option[contains(text(),\"Pruebas QA\")]")
    WebElement PruebasQA_option;

    @FindBy (xpath = "//div/select[@id=\"customfield_15865\"]")
    WebElement select_EquipoResponsable;

    @FindBy (xpath = "//div/select[@id=\"customfield_15865\"]/option[@value=\"22338\"]")
    WebElement Producto_option;

    @FindBy (xpath = "//input[@id=\"assignee-field\"]")
    WebElement input_Responsable;

    @FindBy (xpath = "//input[@value=\"Create\"]")
    WebElement button_Create;

    public Jira(WebDriver driver){
        this.driver= driver;
        PageFactory.initElements(driver, this);
    }

    public void login(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        link_identificacion.click();
        user.sendKeys("nocaball");
        password.sendKeys("Nc052020");
        button_Entrar.click();
    }

    public void completarDatos() throws Exception{

        typeOfRequest_input.click();
        Thread.sleep(6000);
        typeOfRequest_input.sendKeys(Keys.DELETE);
        Thread.sleep(3000);
        typeOfRequest_input.sendKeys("Correctivo");
        Thread.sleep(1000);
        siguiente_button.click();
        input_componente.sendKeys("ehCOS Desarrollo");
        Thread.sleep(2000);
        nombreProyecto_option.click();
        Thread.sleep(2000);
        centro_select.click();
        Thread.sleep(1000);
        ehCOS_QA_option.click();
        Equipo_select.click();
        Thread.sleep(2000);
        producto_option.click();
        select_subtypeRequest.click();
        Thread.sleep(2000);
        Funcional_option.click();
        input_versionAfectada.sendKeys("ehCOS 5.13R");
        select_FaseDetecInc.click();
        Thread.sleep(2000);
        PruebasQA_option.click();
        select_EquipoResponsable.click();
        Thread.sleep(2000);
        Producto_option.click();
        input_Responsable.sendKeys("Usuario Genérico ehCOS");
        button_Create.click();


    }



}
