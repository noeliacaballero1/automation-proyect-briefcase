package suites.CPOE_Clinic;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import java.util.concurrent.TimeUnit;

/*Esta clase corresponde al testcase 25153 de Pruebas Funcionales -> Clinic -> CPOE -> CPOE Clinic
Resumen: Solicitar prueba de de tipo flujo general recién creado y ya existente*/

public class QAEHCOS_25153 {

    WebDriver driver;
    String baseURL = "http://192.168.1.66:8080/ehHIS-ui/front.zul";
    String expected;
    String actual;
    WebDriverWait wait;

    private  void setProperties(String prop, String driverName){
        System.setProperty("webdriver."+ prop + ".driver" ,System.getProperty("user.dir") + "\\drivers\\" +driverName + ".exe");
    }
    @BeforeMethod
    @Parameters ("browser")
    public void LaunchBrowser(String browser) throws Exception{

        switch (browser.toLowerCase()){

            case "chrome":
                setProperties("chrome","chromedriver" );
                driver= new ChromeDriver();
            break;

            case "mozilla":
                setProperties("gecko", "geckodriver");
                driver = new FirefoxDriver();
            break;

            case "explorer":
                setProperties("IEDriverServer", "IEDriverServer");
                driver = new InternetExplorerDriver();
            break;

            case "edge":
                setProperties("edge", "MicrosoftWebDriver");
                driver = new EdgeDriver();
            break;

            default:
                throw new Exception("Incorrect browser");

        }
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(12, TimeUnit.SECONDS);
        driver.get(baseURL);
    }
}
