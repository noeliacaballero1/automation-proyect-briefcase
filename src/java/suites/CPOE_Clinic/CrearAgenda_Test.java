package suites.CPOE_Clinic;

import pages.ConfigDetalleAgenda_page;
import pages.Login_Page;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


public class CrearAgenda_Test {
    WebDriver driver;
    String user = "EHCOS";
    String pass = "EHCOS";
    Login_Page Login_obj;

    @BeforeTest
    private void setupBrowser(){
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\drivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://10.115.88.66:8080/ehCS-ui/");

    }
    @AfterTest (enabled = false)
    private void closeBrowser(){
        driver.close();
    }

    @Test (priority = 1)
    private void IniciarSesion(){
        Login_obj = new Login_Page(driver);
        Login_obj.setUserName(user);
        Login_obj.setPassword(pass);
        Login_obj.clickOnLoginButton();

    }

    @Test (priority = 2)
    private void ConfigAgenda(){

        ConfigDetalleAgenda_page detalleAgenda = new ConfigDetalleAgenda_page(driver);
        detalleAgenda.Entrar_ConfigAgendas();
        detalleAgenda.click_botonNuevo();
        try {
            detalleAgenda.completa_Datos_Oblig();
        }
        catch (Exception e){
            System.out.println("No se puede completar la fecha");
            e.printStackTrace();
        }
    }


}
