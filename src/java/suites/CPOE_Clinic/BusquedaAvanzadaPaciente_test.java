package suites.CPOE_Clinic;

import pages.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class BusquedaAvanzadaPaciente_test {

    WebDriver driver;
    String user="medico.st-es", pass="medico.st-es";
    Login_Page login_obj;
    ListaDePacientes_page listaPacientes_obj;
    PortadaUserMed_Page portada_obj;
    BusquedaAvanzadaPac_Page busquedaPaciente_obj;
    SeleccionDeAgenda_Page seleccionAgenda_obj;
    AgendaCEX_page agendaCEX_obj;

    @BeforeTest
    private void setupBrowser(){
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\drivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        Properties p = new Properties();
        driver.get("http://10.115.88.66:8080/ehCS-ui/");
    }

    @Test(priority = 1)
    private void Login(){
        login_obj = new Login_Page(driver);
        login_obj.login(user, pass);
    }

    @Test (priority = 2)
    private void acceder_Hospitalizacion(){
        portada_obj = new PortadaUserMed_Page(driver);
        portada_obj.seleccionarArea("Hospitalización");
        driver.findElement(By.xpath("//a[contains(text(), \"MED-ST-CENTRO_0-TRAUMA\")]")).click();
    }



    @Test (priority = 3)
    private void Busqueda_Pac() throws Exception{
        busquedaPaciente_obj = new BusquedaAvanzadaPac_Page(driver);
        busquedaPaciente_obj.clickOnBusquedaAvanzadaButton();
        Thread.sleep(3000);
        busquedaPaciente_obj.setApellido("Ruggeri");
        Thread.sleep(2000);
        busquedaPaciente_obj.setSexo("Mujer");
        Thread.sleep(2000);
        busquedaPaciente_obj.clickOnBuscarButton();
        Thread.sleep(3000);
    }




}
