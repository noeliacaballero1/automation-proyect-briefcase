
/*
Resumen:
1- Ingresar un paciente a una agenda del día por medio del botón Cita Urgente.
2- Luego atender al paciente y prescribirle un medicamento, completando los datos básicos del formulario de prescripción.
3 - Confirmar Hoja de Tratamientos.
4 -Volver a la Agenda y marcar al paciente como "atendido".
 */

package suites.OtrasPruebas;

import pages.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;


public class QAMZA_32920 {
    WebDriver driver;
    Login_Page Login_obj;
    PortadaUserMed_Page portada_obj;
    BusquedaAvanzadaPac_Page busquedaPaciente_obj;
    ListaDePacientes_page listaPacientes_obj;
    SeleccionDeAgenda_Page seleccionAgenda_obj;
    AgendaCEX_page agendaCEX_obj;
    Antecedentes_page antecedentes_obj;
    Prescripcion_page prescripcion_obj;
    String user= "medico.st-es", pass= "medico.st-es";


    @BeforeTest
    private void setupBrowser(){
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\drivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("http://10.115.88.66:8080/ehCS-ui/");
    }

    @Test (priority = 5)
    private void seleccionarPaciente() throws Exception{

        listaPacientes_obj = new ListaDePacientes_page(driver);
        listaPacientes_obj.seleccionarPaciente();
        Thread.sleep(2000);
        listaPacientes_obj.cerrarListadePacientes();

    }
    @Test (priority = 1)
    private void login(){
        Login_obj = new Login_Page(driver);
        Login_obj.login(user, pass);
    }

    @Test (priority = 2)
    private void acceder_CEX(){
        portada_obj = new PortadaUserMed_Page(driver);
        portada_obj.seleccionarArea("Consulta Externa");
        driver.findElement(By.xpath("//a[@id=\"zk_comp_44\"]")).click();
    }

    @Test (priority = 4)
    private void Busqueda_Pac() throws Exception{
        busquedaPaciente_obj = new BusquedaAvanzadaPac_Page(driver);
        busquedaPaciente_obj.clickOnBusquedaAvanzadaButton();
        Thread.sleep(3000);
        busquedaPaciente_obj.setApellido("Ruggeri");
        Thread.sleep(2000);
        busquedaPaciente_obj.setSexo("Mujer");
        Thread.sleep(2000);
        busquedaPaciente_obj.clickOnBuscarButton();
        Thread.sleep(3000);
    }

    @Test (priority = 3)
    private void AccederAgenda() throws Exception{
        seleccionAgenda_obj = new SeleccionDeAgenda_Page(driver);
        seleccionAgenda_obj.filtrarBusqueda();
        Thread.sleep(3000);
        seleccionAgenda_obj.seleccionarAgenda();
        Thread.sleep(3000);
    }

    @Test (priority = 6)
    private void citaUrgente(){
        try {
            agendaCEX_obj = new AgendaCEX_page(driver);
            agendaCEX_obj.clickOnButtonCitaUrg();
            Thread.sleep(3000);
            agendaCEX_obj.clickOnButtonAceptar();
        }

        catch (Exception e){

        }

    }

    @Test (priority = 7)
    private void RegistrarAlergeno(){
        try {
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            antecedentes_obj = new Antecedentes_page(driver);
            antecedentes_obj.completarAntecedentes();
            Thread.sleep(5000);
        }

        catch(Exception e){
            e.printStackTrace();
        }
    }


    @Test (priority = 8)
    private void PrescribirFarmaco(){
        try{
            prescripcion_obj = new Prescripcion_page(driver);
            prescripcion_obj.CompletarFormularioPrescripcion();
        }

        catch (Exception e){

        }
    }

}