package suites.OtrasPruebas;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

import java.awt.*;
import java.awt.event.KeyEvent;

public class RobotApi {
    WebDriver driver;

    @Test
    public void robotAPITest() throws Exception{
        System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")+"\\drivers\\geckodriver.exe");
        driver = new FirefoxDriver();

        //Maximizar ventana de Firefox
        driver.manage().window().maximize();

        //
        driver.get("https://www.seleniumeasy.com/test/generate-file-to-download-demo.html");

        //pasarle una cadena de texto a la caja de texto
        driver.findElement(By.xpath("//textarea[@class=\"form-control\"]")).sendKeys("Este es un texto de prueba");

        //Pulsar el botón generate
        driver.findElement(By.xpath("//button[@class=\"btn btn-default\"]")).click();

        //Pulsar el link Download
        driver.findElement(By.xpath("//div/a[contains(text(), \"Download\")]")).click();

        //Creo un robot
        Robot robot = new Robot();

        Thread.sleep(2000);
        //Ir un radio box más abajo en la ventana de descarga para pulsar "Guardar archivo"
        robot.keyPress(KeyEvent.VK_DOWN);

        Thread.sleep(2000);
        //Pulsar Tab 3 veces
        for (int i=0; i<3; i++){
            robot.keyPress(KeyEvent.VK_TAB);
        }

        Thread.sleep(2000);
        //Pulsar Aceptar (Enter)
        robot.keyPress(KeyEvent.VK_ENTER);

        Thread.sleep(2000);
        driver.quit();


    }
}
