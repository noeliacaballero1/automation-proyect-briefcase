package suites.OtrasPruebas;

import pages.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class CrearIncidenciaJira {

    WebDriver driver;

    Jira jira_obj;


    @BeforeTest
    private void setupBrowser(){
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\src\\resources\\drivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        Properties p = new Properties();
        driver.get("https://steps.everis.com/jiraesp1/secure/CreateIssue!default.jspa");
    }

    @Test (priority = 0)
    private void loginOnJira(){
        jira_obj= new Jira(driver);
        jira_obj.login();
    }

    @Test (priority = 1)
    private void CompletarDatos(){


        try{
            jira_obj= new Jira (driver);
            jira_obj.completarDatos();
        }

        catch(Exception e){
            e.printStackTrace();
        }

    }

}
