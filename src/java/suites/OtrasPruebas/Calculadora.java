package suites.OtrasPruebas;

import javax.swing.*;

public class Calculadora {


    double resultado;



    public Float sumar(){
        String a = JOptionPane.showInputDialog("Introduce el primer número: ");
        Float a1 = Float.parseFloat(a);

        String b = JOptionPane.showInputDialog("Introduce el segundo número");
        Float b1 = Float.parseFloat(b);

        Float resultado = a1+b1;

        return resultado;


    }

    private Float restar(){
        String a = JOptionPane.showInputDialog("Introduce el primer número: ");
        Float a1 = Float.parseFloat(a);

        String b = JOptionPane.showInputDialog("Introduce el segundo número");
        Float b1 = Float.parseFloat(b);

        Float resultado = a1-b1;

        return resultado;
    }

    private float multiplicar(){
        String a = JOptionPane.showInputDialog("Introduce el primer número: ");
        Float a1 = Float.parseFloat(a);

        String b = JOptionPane.showInputDialog("Introduce el segundo número");
        Float b1 = Float.parseFloat(b);

        Float resultado = a1*b1;

        return resultado;
    }

    private float dividir(){
        String a = JOptionPane.showInputDialog("Introduce el primer número: ");
        Float a1 = Float.parseFloat(a);

        String b = JOptionPane.showInputDialog("Introduce el segundo número");
        Float b1 = Float.parseFloat(b);

        try {
            float resultado = a1 / b1;

        }
        catch(Exception e){
            System.out.println("Error de división por cero");
        }
        return (float) resultado;
    }

    public static void main (String args[]){
        Calculadora c1 = new Calculadora();
        String operacion = JOptionPane.showInputDialog("Introduce la operación: m, d, s, r");
        switch (operacion){
            case "s":
                c1.sumar();

            case "r":
                c1.restar();

            case "m":
                c1.multiplicar();

            case "d":
                c1.dividir();
        }
    }
}
